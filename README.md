# stage-2-es6-for-everyone

## Instalation

`npm install`

`npm run build`

`npm run start`

open http://localhost:8080/

## version on branch develop

connects to street-fighter-server

repository [fighter-server](https://bitbucket.org/artem-prydorozhko/fighters-server/src/master/)

deployed [street-fighter-server](https://street-fighters-server.herokuapp.com)

+ gets all info about heroes from server

+ when heroes info is changed it is saved on server