import View from './view';

export default class Fighter{

    constructor(fighter){
        this.id = fighter._id;
        this.name = fighter.name;
        this.health = fighter.health;
        this.attack = fighter.attack;
        this.defense = fighter.defense;
        this.source = fighter.source;
    }

    getHitPower() {
        let criticalHitChance = Math.floor(Math.random()*2+1);
        return this.attack*criticalHitChance;
    }
    getBlockPower() {
        let dodgeChance = Math.floor(Math.random()*2+1);
        return this.defense*dodgeChance;
    }

    createFighter(num) {
        let view = new View();
        const imageElement = view.createElement({
            tagName: 'img',
            className: `fighter-image${num}`,
            attributes: { src: this.source }});
        const nameElement = view.createElement({ tagName: 'span', className: 'name' });
        nameElement.innerText = this.name;
        const heathElement = view.createElement({ tagName: 'div', className: `health${num}` });
        heathElement.innerText = this.health;
    
        let element = view.createElement({ tagName: 'div', className: `fighter${num}` });
        element.append(imageElement, nameElement, heathElement);
        return element;
    }

   async Hit(attackerImg,opponentImg, opponent,resolve,num) {
    

        const attack = (function(){attackerImg.classList.add(num==1?'attackright':'attackleft');}).bind(this);
        const stepback = (function () {
            attackerImg.classList.remove(num==1?'attackright':'attackleft'); 
            opponentImg.style.cssText="background: rgba(250,0,0,0.4);"
            const hitpower = this.getHitPower()-opponent.getBlockPower();
            opponent.health -= hitpower>0?hitpower:0;
            if(opponent.health<0)
            opponent.health = 0;
        }).bind(this);
        setTimeout(attack,1000);
        setTimeout(stepback,1300);
        setTimeout(comeback,1400);

        function comeback (){opponentImg.style.cssText="";resolve(opponent.health)}
    }
}