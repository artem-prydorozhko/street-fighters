import View from './view';
import FighterView from './fighterView';

import {fighterService} from './services/fightersService';

class FightersView extends View {
  constructor(fighters) {
    super();

    this.handleClick = this.handleFighterClick.bind(this);
    this.handleChooseClick = this.handleChooseFighterClick.bind(this);
    this.handleCancelClick = this.handleCancelFighterClick.bind(this);
    this.handleNoClick = this.handleNoFightClick.bind(this);
    this.createFighters(fighters);

    this.closeButton = document.getElementById('modal-ok');
    this.chooseButton = document.getElementById('modal-choose');
    this.cancelButton = document.getElementById('modal-cancel');
    this.modalElem = document.getElementById('modal');  
    this.overlay = document.getElementById('overlay-modal');
    this.health = document.getElementById('modal-health');
    this.attack = document.getElementById('modal-attack');
    this.defense = document.getElementById('modal-defense');
    this.name = document.getElementById('modal-name');
    this.modalConfirm = document.getElementById('modal-confirm');
    this.noButton = document.getElementById('modal-no');

    this.closeButton.addEventListener('click', (e)=> {
      this.fightersDetailsMap.get(this.fighterid).health = this.health.value;
      this.fightersDetailsMap.get(this.fighterid).attack = this.attack.value;
      this.fightersDetailsMap.get(this.fighterid).defense = this.defense.value;
      // console.log(this.fighterid);
      this.modalElem.classList.remove('active');
      this.overlay.classList.remove('active');
   });

    this.chooseButton.addEventListener('click',(event)=>this.handleChooseClick());
    this.cancelButton.addEventListener('click',(event)=>this.handleCancelClick());
    this.noButton.addEventListener('click',(event)=>this.handleNoFightClick());
  }

  fightersDetailsMap = new Map();
  chosenFighters = new Map();   // fighters to fight

  fighterinfo;
  fighterid;

  modalConfirm;
  noButton;
  // info in modal window
  modalElem;
  overlay;
  name;
  health;
  attack;
  defense;
  closeButton;
  chooseButton;
  cancelButton;

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });
    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
    // console.log(fighters);
    for(let fighter of fighters) {
      this.getInfo(fighter._id);
    }
  }

  async handleFighterClick(event, fighter) {
    this.fighterid = fighter._id;

    // if there is no info in map then load
    if(!this.fightersDetailsMap.has(fighter._id)) {
      let helpfunc = (function(result){this.fighterinfo = result;}).bind(this);
      await fighterService.getFighterDetails(fighter._id).then(helpfunc);
      this.fightersDetailsMap.set(fighter._id, this.fighterinfo);
    } else {
      this.fighterinfo = this.fightersDetailsMap.get(fighter._id);
    }


    console.log(this.fightersDetailsMap);
    this.name.innerHTML = this.fighterinfo.name;
    this.health.value = this.fighterinfo.health;
    this.attack.value = this.fighterinfo.attack;
    this.defense.value = this.fighterinfo.defense;
    this.modalElem.classList.add('active');
    this.overlay.classList.add('active');

    if(this.chosenFighters.has(this.fighterid)) {
      this.chooseButton.disabled = true;
      this.cancelButton.disabled = false;
    } else {
      this.chooseButton.disabled = false;
      this.cancelButton.disabled = true;
    }


    // get from map or load info and add to fightersMap
    // show modal with fighter info
    // allow to edit health and power in this modal
  }

  async getInfo(id) {
    let helpfunc = (function(result){this.fighterinfo = result;}).bind(this);
    await fighterService.getFighterDetails(id).then(helpfunc);
    this.fightersDetailsMap.set(id, this.fighterinfo);
  }

  handleChooseFighterClick() {
    this.chosenFighters.set(this.fighterid,this.fighterinfo);
    this.closeButton.click();
     console.log(this.chosenFighters);
    let fighters = document.getElementsByClassName('fighter');
    for(let elem of fighters) {
      if(elem.lastChild.textContent == this.fighterinfo.name) {
        elem.style.cssText="background: rgba(66, 244, 89, 0.5)";
        break;
      }
    }
    
    if(this.chosenFighters.size === 2) {
      this.modalConfirm.classList.add('active');
      this.overlay.classList.add('active');
    }
  }

  handleCancelFighterClick() {
    
    this.chosenFighters.delete(this.fighterid);
    this.closeButton.click();
    console.log(this.chosenFighters);
    let fighters = document.getElementsByClassName('fighter');
    for(let elem of fighters) {
      if(elem.lastChild.textContent == this.fighterinfo.name) {
        elem.style.cssText="";
        break;
      }
    }
  }

  handleNoFightClick() {
    let fighters = document.getElementsByClassName('fighter');
    for(let elem of fighters) {
        elem.style.cssText="";
    }
    this.chosenFighters.clear();
    this.modalConfirm.classList.remove('active');
    this.overlay.classList.remove('active');
  }
}

export default FightersView;