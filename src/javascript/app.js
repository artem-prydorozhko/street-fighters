import FightersView from './fightersView';
import FighterView from './fighterView';
import Fighter from './Fighter';
import { fighterService } from './services/fightersService';

class App {
  constructor() {
    this.startApp();
    this.handleClick = this.handleYesClick.bind(this);
    App.modalYes.addEventListener('click',(event)=>this.handleClick());
  }

  static rootElement = document.getElementById('root');
  static loadingElement = document.getElementById('loading-overlay');
  static modalConfirm = document.getElementById('modal-confirm');
  static overlay = document.getElementById('overlay-modal');
  static modalYes = document.getElementById('modal-yes');
  static fighting = document.getElementById('fighting');
  fightersView;

  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';
      
      const fighters = await fighterService.getFighters();
      this.fightersView = new FightersView(fighters);
      const fightersElement = this.fightersView.element;

      App.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }

  async fight(fighters) {
    App.rootElement.style.visibility = 'hidden';
    const tempfighters = fighters.values();
    let fighter = [];
    for(let elem of tempfighters) {
      fighter.push(new Fighter(elem));
    }
    console.log(fighter);

    
    let fighterElement1 = fighter[0].createFighter(1);
    let fighterElement2 = fighter[1].createFighter(2);
    console.log(fighterElement1, fighterElement2);
    App.fighting.appendChild(fighterElement1);
    App.fighting.appendChild(fighterElement2);
    App.fighting.style.visibility = 'visible';
    const fighterImg1 = document.getElementsByClassName('fighter-image1')[0];
    const fighterImg2 = document.getElementsByClassName('fighter-image2')[0];
    const fighter1health = document.getElementsByClassName('health1')[0];
    const fighter2health = document.getElementsByClassName('health2')[0];
    let func1 = fighter[0].Hit.bind(fighter[0]);
    let func2 = fighter[1].Hit.bind(fighter[1]);
    
    while(fighter2health.innerHTML > 0 && fighter1health.innerHTML > 0){
      let promise = new Promise(function(resolve,reject){func1(fighterImg1,fighterImg2,fighter[1],resolve,1)});
      let temp = await promise.then(function(result){fighter2health.innerHTML = result});
      if(fighter2health.innerHTML <= 0 || fighter1health.innerHTML <= 0)
        break;
      let promise2 = new Promise(function(resolve,reject){func2(fighterImg2,fighterImg1,fighter[0],resolve,2)});
      let temp2 = await promise2.then(function(result){fighter1health.innerHTML = result});
    }

    if(fighter2health.innerHTML <= 0) {
      App.fighting.removeChild(fighterElement2);
    } else {
      App.fighting.removeChild(fighterElement1);
    }

    let win = document.getElementById('win');
    win.classList.add('active');
  }

  handleYesClick() {
    App.modalConfirm.classList.remove('active');
    App.overlay.classList.remove('active');
    this.fight(this.fightersView.chosenFighters);
  }
}

export default App;